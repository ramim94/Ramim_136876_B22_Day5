<?php

$search= "brown fox";
$replace= "blue fox";
$subject="the brown fox jumped over the wall";

echo "original string: <br>";

echo $subject;
echo "<hr>";

echo "replaced string: <br>";

echo str_replace($search,$replace,$subject);


/*This function returns a string or an array with all occurrences
of search in subject replaced with the given replace value.*/