<?php

$string="this fox tests how many fox times the string fox occurs in this fox";

echo substr_count($string,"fox");

/**
 * substr_count() returns the number of times the needle substring occurs in the haystack string.
 * Please note that needle is case sensitive.

haystack
The string to search in

needle
The substring to search for
 */