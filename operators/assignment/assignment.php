<?php

$a=5;
$b=($a+2)+$a;

echo $b."<br>";

$a += 8; // same as a=a+8
$b = "Hi ";
$b .= "world"; // concatanates first $b and second $b

echo $a."<br>";

echo $b;

/**
 * = is used as assignment operator, since it is used to assign values
 */