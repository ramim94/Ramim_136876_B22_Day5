<?php
//type 1: and

$a=2;$b=9;

if($a==2 and $b==9){
    echo "both statements were true <br>";
}else{
    echo "none was true";
}
//type 2: &&
if($a==2 && $b==9){
    echo "both statements were true <br>";
}else{
    echo "none was true";
}


/**
 * there are two types: 1. and 
 *                      2. &&
 */