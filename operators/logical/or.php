<?php
//type 1: or

$a=2;$b=9;

if($a==2 or $b==9){
    echo "either statements were true <br>";
}else{
    echo "none was true";
}
//type 2: ||
if($a==2 || $b==9){
    echo "either statements were true <br>";
}else{
    echo "none was true";
}

//difference between || and && is for or, it'll be true if any one is true:
echo"<br><br> checking difference between and or<br><br>";

$a=2;$b=4;

if($a==2 or $b==9){
    echo "either statements were true <br>";
}else{
    echo "none was true";
}


/**
 * there are two types: 1. or
 *                      2. ||
 */