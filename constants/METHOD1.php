<?php

class properties{
    public function username(){
        echo "MOHAIMANUL HOQUE CHOWDHURY";
    }
    public function methodNameUsed(){
        echo __METHOD__;
    }
    public function functionNameUsed(){
        echo __FUNCTION__;
    }

}

$obj=new properties();

$obj->username();
echo" created 3 methods, when used __METHOD__ we get: <br>";
$obj->methodNameUsed();
echo "<br> and if __FUNCTION__ constant is used then: <br>";
$obj->functionNameUsed();

/**
 using __METHOD__
 */