<?php

class MYclass{
    public function firstmethod(){
        echo "the name of this method is ".__METHOD__;
    }
}

$obj=new MYclass();

$obj->firstmethod();

/**
 any function inside a class is called method, when used __METHOD__ it shows the name of the class
 * as well as the name of the function
 */